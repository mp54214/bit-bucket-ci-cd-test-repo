//const MongoClient = require('mongodb').MongoClient;

const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.send('Hello World! -v2 -samu plus p-hukka - 5566666 --67 ');
});

app.listen(3000, () => {
  console.log(`Server started on port ${3000}`);
});

// addition
const addition = (a, b) => {
  return a + b;
}

// subtraction
const subtraction = (a, b) => {
  return a - b;
}

// multiplication
const multiplication = (a, b) => {
  return a * b;
}

// division
// this is division
const division = (a, b) => {
  return a / b;
}

// Connection URL

//const url = 'mongodb://localhost:27017';

//MongoClient.connect(url, (err, client) => {
//  if (err) {
//    console.log("there's been an error");
//  }
//  console.log("it's connected!");

//  const db = client.db('TestDB');

//  db.collection('Test').insertOne({
//    text: 'something to do'
//  }, (err, result) => {
//    if (err) {
//      return console.log('unable to create', err)
//    }

//    console.log(JSON.stringify(result.ops, undefined, 2));
//  })

//  client.close();

//
//});


module.exports = {
  addition, subtraction, multiplication, division
}
